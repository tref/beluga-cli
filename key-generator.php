<?php

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

require "vendor/autoload.php";


// Do this once then store it somehow:
$key = Key::loadFromAsciiSafeString('def00000a42937f978d577ebb9550a45f6a71701886f7f6e9769a61a3582e4584747d987a10998f3e34182cbd83e01b7883bd7401e4ce653c6f8ed8d8698004cd5ced919');
$encrypted = Defuse\Crypto\Crypto::encrypt('blabla', $key);
$decrypted = Defuse\Crypto\Crypto::decrypt('def50200805d405f68806bcd25b46b76ffa3709add9adb557996b417372c8c930d7f53670436003a9fa69361cc66a00a2296032a01dc71d8f3737341ec6853554efafc9a0c432c2d0c88d362a1e0f60b8c18c3cc7ed873a4fcd78a939e6c7ac9050028d6083d127adb5d9c8b43d2a1d82b45e794bed9ebe7eacf8ad07bcc6a9ea07cabaf68e0', $key);
var_dump($decrypted);
