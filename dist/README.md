# 'dist' directory

This directory contains installer file(s) for the CLI.

These files are not packaged nor used directly by the CLI. Instead, they are
copied to the main Beluga website under these URLs:

```
https://cli.ref-belu.ga/installer
https://cli.ref-belu.ga/manifest.json
```
