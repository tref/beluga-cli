<?php

namespace Beluga\Cli\Util;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

class EncryptUtil {

  public static function encrypt($plaintext, $string) {
    $key = Key::loadFromAsciiSafeString($string);
    return Crypto::encrypt($plaintext, $key);
  }

  public static function decrypt($ciphertext, $string) {
    $key = Key::loadFromAsciiSafeString($string);
    return Crypto::decrypt($ciphertext, $key);
  }

}