<?php

namespace Beluga\Cli\Command;

use Platformsh\Cli\Service\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CryptoBaseCommand extends BaseCommand
{
  protected $local = true;

  protected function execute(InputInterface $input, OutputInterface $output) {
    parent::execute($input, $output);
  }

  protected function getKey() {
    $private_file = Filesystem::getHomeDirectory() . '/.ssh/' . $this->config()->get('application.secret_key_file');

    if (!file_exists($private_file)) {
      $this->stdErr->writeln("Unable to find the private file in location <error>$private_file</error>. Make sure it exists.");
      return 1;
    }
    return rtrim(file_get_contents($private_file));
  }

}
