<?php

namespace Beluga\Cli\Command;

use Platformsh\Cli\Command\CommandBase;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BaseCommand extends CommandBase {

  protected function execute(InputInterface $input, OutputInterface $output) {
    $fs = new \Symfony\Component\Filesystem\Filesystem();
    $config_dir = $this->config()->getUserConfigDir();
    if (!file_exists($config_dir)) {
      $this->stdErr->writeln("Your private beluga folder does not yet exist, creating it now.");
      $fs->mkdir($config_dir);
    }
  }

}