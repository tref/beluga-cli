<?php

namespace Beluga\Cli\Command;

use Beluga\Cli\Util\EncryptUtil;
use Platformsh\Cli\Command\CommandBase;
use Platformsh\Cli\Service\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetSecretCommand extends CryptoBaseCommand
{
  protected $local = true;

  protected function configure()
  {
    $this
      ->setName('secret')
      ->addArgument('key', InputArgument::REQUIRED, 'The key of the secret you want to see')
      ->setDescription('Gets a secret value');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    parent::execute($input, $output);

    if (!array_key_exists($input->getArgument('key'), $this->config()->get('secrets'))) {
      $this->stdErr->writeln("Secret <error>" . $input->getArgument('key') . "</error> has not been specified in the config file.");
      return 1;
    }
    $ciphertext = $this->config()->get('secrets.' . $input->getArgument('key'));
    $this->stdErr->writeln(EncryptUtil::decrypt($ciphertext, $this->getKey()));
  }

}
