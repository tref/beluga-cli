<?php

namespace Beluga\Cli\Command;

use Beluga\Cli\Util\EncryptUtil;
use Platformsh\Cli\Command\CommandBase;
use Platformsh\Cli\Service\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DecryptCommand extends CryptoBaseCommand
{
  protected $local = true;

  protected function configure()
  {
    $this
      ->setName('decrypt')
      ->addArgument('string', InputArgument::REQUIRED, 'The string you want to decrypt')
      ->setDescription('Decrypt a message');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    parent::execute($input, $output);
    $this->stdErr->writeln(EncryptUtil::decrypt($input->getArgument('string'), $this->getKey()));
  }

}
